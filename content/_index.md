---
title: Asqatasun, webpage analyser for SEO and Accessibility
description: Opensource software. Speed your accessibility and SEO testing width Asqatasun. Asqatasun automates SEO tests and accessibility tests (RGAA 3). Evaluating a page, an entire site or a web application is reliable and intuitive.
---


## Built with reliability in mind

Asqatasun is the leading opensource software for web accessibility (#a11y) since 2007. 
Built with reliability in mind, it also addresses SEO concerns, and is extensible 
to any other domain.

Asqatasun provides a huge level of automation and can be included 
in Continuous Integration thanks to its Jenkins Plugin.

- First, start with the README, which explains everything.
- If you have any question or want to discuss with people, come to the Asqatasun forum.
- Download Asqatasun or use Asqatasun with Docker (even on Windows !)

