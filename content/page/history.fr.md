+++
title       = "À propos d'Asqatasun"
+++

## L'histoire d'Asqatasun

Asqatasun a été créé en 2007 par Matthieu Faure, expert en accessibilité du web. Alors nommé **EvalAccess**, il a pris le nom de **Tanaguru** quand Matthieu créa la start-up pour vendre du service autour du logiciel libre. La société a embauché Jérôme Kowalczyk comme <em lang="en">Lead Developer</em> pour étendre le logiciel et a eu de prestigieux clients comme la Ville de Paris, Pôle-Emploi ou encore l’INSEE.

Mais la start-up n’a pas réussi à trouver un modèle économique viable et a fermé fin 2014, perdant alors la marque du produit. **Asqatasun** est le fork de Tanaguru par ses créateurs Matthieu et Jérôme en 2015, vite rejoints par Fabrice Gangler, libriste de longue date et expert <span lang="en"><abbr title="Search Engine Optimisation">SEO</abbr></span>.

Asqatasun a pour vocation de fournir un **véritable logiciel libre**, ouvert, [documenté](https://doc.asqatasun.org/), tourné vers ses utilisateurs et [sa communauté](https://forum.asqatasun.org/) grandissante.

Asqatasun est un néologisme issu du basque et signifie liberté, traçant ainsi une belle trajectoire pour un logiciel libre !