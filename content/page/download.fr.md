---
title:  "Télécharger Asqatasun"
subtitle: "Binaires, image Docker, code source"
---

## Dernière version

[Télécharger Asqatasun v4.0.3 (80 <abbr title="Mega-Octets">Mo</abbr>)](https://github.com/Asqatasun/Asqatasun/releases/download/v4.0.3/asqatasun-4.0.3.i386.tar.gz) 2016-08-22

## Image Docker

Attention: L'image Docker n'est **pas** destinée à être utilisée en production, mais vous pouvez jouer avec :)

* [Image Docker Asqatasun](https://hub.docker.com/r/asqatasun/asqatasun/)
* DOC: [specificités de l'image Docker Asqatasun](https://doc.asqatasun.org/en/10_Install_doc/Docker/index.html)

## Code source

[github.com/Asqatasun/Asqatasun](https://github.com/Asqatasun/Asqatasun/)

Toute [contribution]({{< relref "community.fr.md" >}}) est la bienvenue !