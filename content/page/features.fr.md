+++
title       = "Fonctionnalités"
subtitle    = "Audit d'accessibilité web / audit technique SEO"
comments    = false
+++

## Vision

1. Être un véritable [logiciel libre](https://github.com/Asqatasun/Asqatasun/blob/develop/LICENSE), entièrement [documenté](https://doc.asqatasun.org/en/)
2. Automatiser tout ce que l'on peut (et faire gagner du temps)
3. Être fiable à 200% (ne pas donner de résultat erroné)
4. Se faire plaisir technoologiquement :)

![](/img/screenshot_20150307_ASQATASUN_5_types_of_result.png)

## Audit d'accessibilité web: `#a11y`, <abbr title="Référentiel Général d'Accessibilité des Administrations">RGAA</abbr>

- auditer une page donnée, et compléter manuellement le résultat pour produire un rapport
- auditer un fichier HTML (un gabarit en cours de construction par exemple)
- auditer un site entier (<em lang="en">crawler</em>
- Auditer un un parcours utilisateur : inscription sur un site, de e-commerce, formulaire administratfif avec les **scenarios** Asqatasun.

## Quels tests d'accessibilité sont couverts ?

* tous les tests relatifs aux balises et attributs comme les `alt`, `th`, `title`, `h1`...
* contraste de couleur
* specification de langue
* fichiers téléchargeabless / documents bureautiques
* changements de contexte
* ...

En juin 2017, ceci représente **173 tests d'accessibilité**.

## Audit technique <span lang="en"><abbr title"Search Engine Optimisation">SEO</abbr></span>

- lancer des tests entièrement automatisés pour débusquer les problèmes SEO
- parcourir des millions de pages
- créer ses propres tests

Quels sont les tests implémentés :

* au niveau du site entier : les duplications de `<h1>`, `<title>`, `<meta description>`, pages ; la presence de robots.txt / sitemap.xml
* au niveau de la page : la non-pertinence des `<h1>`..`<h6>`, `<title>`, `<meta description>`, contenus de liens `<a href="">...</a>` ; presence de <span lang="en">rewrite-rule</span>

## Internationalisation

Asqatasun est traduit en trois langues :

* Français
* Anglais
* Espagnol
