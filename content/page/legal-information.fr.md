+++
title = "Mentions légales"
subtitle = "Mentions légales"
description = "TODO"
comments = false
+++

## Crédits

- Logo made with love by Tiscia
- Theme by [Beautiful Jekyll](http://deanattali.com/beautiful-jekyll/) adapted to [Beautiful Hugo](https://github.com/halogenica/beautifulhugo)
- [Hugo](http://gohugo.io/) powered (static site generator)
- GiLab CI
 
 
## Hébergeur

OVH
www.ovh.com
2 rue Kellermann
59100 Roubaix - France

## Responsable de publication

## Copyright / Droits d'Auteur

## Données personnelles

Conformément à la loi française "Informatique et Libertés" du 6 Janvier 1978, 
les informations fournies par l'utilisateur à Asqatasun.org sont strictement confidentielles. 
L'utilisateur dispose d'un droit d'accès, de modification, de rectification 
et de suppression des données qui le concernent. 
Il peut exercer ce droit en adressant un courrier à : 


