+++
title ="Features"
subtitle = "Web accessibility audit / SEO technical audit"
comments = false
+++

## Vision

1. Be a truly [libre and open-source](https://github.com/Asqatasun/Asqatasun/blob/develop/LICENSE) software, fully [documented](https://doc.asqatasun.org/en/)
2. Automate as much as we can
3. Be 200% reliable (don't give erroneous result)
4. Have technological fun :)

![](/img/screenshot_20150307_ASQATASUN_5_types_of_result.png)

## Web accessibility assessment: `#a11y`, </span lang="fr"><abbr title="Référentiel Général d'Accessibilité des Administrations">RGAA</abbr></span>, <abbr title="Web Content Accessibility Guidelines">WCAG</abbr>

- scan a given page, and manually fulfill the audit to produce report
- scan offline file (e.g. template being created but not online yet)
- scan a whole site for accessibility issues (crawler included)
- scan a user-workflow like site registration, 
  form completion or e-commerce checkout with Asqatasun **scenarios**.
  
## What accessibility tests are covered ?

* all the "tag and attributes tests" like missing alt, table headers check, frame title...
* color contrast
* language specification
* downloadable files / office files (spreadsheet, word-processor...)
* switch of context
* ...

As of June 2017, this represents **173 accessibility tests**.

## <abbr title"Search Engine Optimisation">SEO</abbr> technical audit

- run fully automated tests to track SEO issues
- scan zillions of pages
- create your own tests

What tests are covered:

* at the scope of the entire site (i.e. site-wide): non-uniqueness (duplicate) of `<h1>`, `<title>`, `<meta description>`, pages ; presence of robots.txt / sitemap.xml
* at the scope of the page: non-relevancy of content of `<h1>`, `<title>`, `<meta description>`, content of link-text `<a href="">...</a>`, `<h1>`...`<h6>` structure ; rewrite-rule presence

## Internationalisation

Asqatasun is translated in three languages:

* English
* French
* Spanish
