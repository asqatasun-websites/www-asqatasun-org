+++
title   = "Communauté"
slug    = "communaute"
+++

## Documentation

La [Documentation Asqatasun](https://doc.asqatasun.org/en/) est disponible, et en particulier :

* le [guide d'installation](https://doc.asqatasun.org/en/index.html) détaillant comment installer et configurer ;
* la [doc utilisateur](https://doc.asqatasun.org/en/20_User_doc/index.html) précisant (entre autres) les 5 types de résultats et les 4 portées d'audit ;
* la [doc contributeur](https://doc.asqatasun.org/en/30_Contributor_doc/index.html) expliquant comment compiler, tester, débugguer à partir du code source, aussi comment créer son propre référentiel, sans oublier le [modèle de données d'Asqatasun](https://doc.asqatasun.org/en/30_Contributor_doc/Engine/Engine_data_model.html) ;
* la  [doc administrateur](https://doc.asqatasun.org/en/30_Contributor_doc/index.html) pour les opérations au quotidien de l'administrateur ;
* la [doc des règles](https://doc.asqatasun.org/en/90_Rules/index.html) détaillant précisement les algorithmes utilisés pour les audits.

## Forum

Vous pouvez aussi poser vos questions et participer sur

* le [Forum Asqatasun](https://forum.asqatasun.org/)

## Code source

Le code source est sur [github.com/Asqatasun/Asqatasun/](https://github.com/Asqatasun/Asqatasun/)

## Comment donner un coup de pouce ?

1. Utilisez  Asqatasun :)
2. Suivez [notre compte Twitter](https://twitter.com/asqatasun)
3. Saisissez des [bugs](https://github.com/Asqatasun/Asqatasun/issues)
4. Faites une <span lang="en">Merge Request</span>, toute [contribution](https://github.com/Asqatasun/Asqatasun/blob/v4.0.3/CONTRIBUTING.md) est la bienvenue !
