+++
title = "About Asqatasun"
+++

## History of Asqatasun

Asqatasun was created back in 2007 by Matthieu Faure, web accessibility expert. Called **EvalAccess** to begin with, it took the name of **Tanaguru** when Matthieu created a start-up to provide commercial support. The company then hired Jérôme Kowalczyk as Lead Developer to extend the software and gained various famous clients such as City of Paris or French electricity provider EDF.

But the start-up did not succeed in building a sustainable business and closed in 2014, loosing the brand of the product. **Asqatasun** is the fork of Tanaguru by its creators Matthieu and Jérôme, quickly joined by Fabrice Gangler a long-time open-source software fellow and <abbr title="Search Engine Optimisation">SEO</abbr>specialist.

Asqatasun is willing to provide a **true open-source software**, fully open and [documented](https://doc.asqatasun.org/), and geared toward its users and [its growing community](https://forum.asqatasun.org/).

Asqatasun comes from a basque word meaning liberty, thus tracing a nice path for a free software.