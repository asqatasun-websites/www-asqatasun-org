+++
title   = "Community"
+++

## Documentation

You can browse the [Documentation for Asqatasun](https://doc.asqatasun.org/en/), especially:

* the [installation guide](https://doc.asqatasun.org/en/index.html) with data on how to install, tune and configure;
* the [user doc](https://doc.asqatasun.org/en/20_User_doc/index.html) with (amongst other things) information on the 5 types of results, the 4 kinds of audits;
* the [contributor doc](https://doc.asqatasun.org/en/30_Contributor_doc/index.html) explaining how to build from source, debug, test, and how to create your own referential, and also the [data model of Asqatasun](https://doc.asqatasun.org/en/30_Contributor_doc/Engine/Engine_data_model.html);
* the [Administrator doc](https://doc.asqatasun.org/en/30_Contributor_doc/index.html) for day-to-day admin stuff;

the [rules doc](https://doc.asqatasun.org/en/90_Rules/index.html) being precise about all the algorithms used for the audits.

## Forum

And also you can ask your questions and participate on:

* the [Asqatasun Forum](https://forum.asqatasun.org/)

## Source code

Source code is available at [github.com/Asqatasun/Asqatasun/](https://github.com/Asqatasun/Asqatasun/)

## How to help ? (Contributing)

1. Use Asqatasun :)
2. Follow [our Twitter account](https://twitter.com/asqatasun)
3. Fill in [bugs](https://github.com/Asqatasun/Asqatasun/issues)
4. Make a Merge Request, any [contribution](https://github.com/Asqatasun/Asqatasun/blob/v4.0.3/CONTRIBUTING.md) is warmly welcomed !
